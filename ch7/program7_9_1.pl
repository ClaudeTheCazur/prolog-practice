room(helen, f6).
room(frank, e6).
room(paul, f9).

phone(e10, 231).
phone(e12, 233).
phone(e11, 244).

visiting(alan, helen).
visiting(liam, paul).
visiting(jane, alan).

find(Person, Room):-
	room(Person, Room).

find(Person, Room):-
	visiting(Person, Visitee),
	room(Visitee, Room).

ring(Person, Number):-
	find(Person, Room),
	phone(Room, Number).

go:-
	find_who, nl,
	write('would you like to find someone else'), nl,
	read(Response), nl,
	check(Response), nl.

find_who :-
	write('Who would you like to find?'), nl,
	read(Person), nl,
	find(Person, Room),
	write('You can find them at '),
	write(Room).

find_who:-
	write('don\'t think they\'ve checked in yet...').

check(Response):-
	Response = yes,
	go.

check(Response):-
	write('Okay, have a pleasant day').

