/*playing_with_lists.pl must be consulted also for this to work*/

/*algorithm to shift list left by X. Faster version*/
shiftLeftV1([], [], _).
/*Actually doesn't need to be here because mod check*/
/*shiftLeftV1([X], [X], _):-
    atom(X).*/
shiftLeftV1(X, Y, Count):-
    length(X, L),
    TrueCount is mod(Count, L),
    slicebyCount(X, X1, X2, TrueCount),
    concat(X2,X1, Y).

slicebyCount(X, [], X2, 0):-
    X2 = X.
slicebyCount([H|T], [H|X1], X2, Count):-
    Count > 0,
    NewCount is Count - 1,
    slicebyCount(T, X1, X2, NewCount).

/*A more precise way of writing shift left by X amount*/
shiftLeftV2([],[], _).
shiftLeftV2(X,X, 0).
shiftLeftV2([H|T], L2, N):-
    concat(T, [H], Y),
    N1 is N -1,
    shiftLeftV2(Y, L2, N1).



/*Combining V1 and V2*/
shiftLeft([], [], _).
shiftLeft(X, Y, Count):-
    length(X, L),
    TrueCount is mod(Count, L),
    shiftLeftV2(X, Y, TrueCount).

smartReverse([X], X).
smartReverse([H|T], [N|[H]]):-
    smartReverse(T, N).

/*reverse X --> Z :
    (smartReverse X -->Y),
    (flatten Y --> Z)
*/