A repository filled with practice programs I've developed on my journey of learning Prolog.

The current book I'm using as a study tool is ***Artificial Intelligence Programming in Prolog by*** Tim Smith.

Majority of the initial programs will be solutions to exercises in this book.