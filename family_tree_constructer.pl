is_parent(Potential_parent, Child):-
	is_father(Potential_parent, Child),
	Potential_parent \= Child.
	

is_parent(Potential_parent, Child):-
	is_mother(Potential_parent, Child),
	Potential_parent \= Child.

are_siblings(Child_A, Child_B):-
	is_parent(Potential_parent, Child_A),
	is_parent(Potential_parent, Child_B),
	Child_A \= Child_B.

is_grandparent(Potential_grandparent, Child):-
	is_parent(Potential_grandparent, Potential_Parent),
	is_parent(Potential_Parent, Child).

are_related(X, Y):-
	is_grandparent(X,Y).

are_related(X,Y):-
	is_grandparent(Y,X).

are_related(X,Y):-
	is_grandparent(X,Y).

are_related(X,Y):-
	are_sibling(X,Y).

are_related(X,Y):-
	is_parent(X,Y).

are_related(X,Y):-
	is_parent(Y,X).

