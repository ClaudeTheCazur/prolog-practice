foo([],[]).

foo([H|T], [X|Y]):-
	H = X,
	foo(T,Y).

/* Decide element is a member of a list*/


member(A, [A|_]).

member(A, [_|Y]):-
	member(A,Y).
/*
member(A, List):-
	write('element is not a member of list provided').
*/
/*Print all elements of list on separate lines*/

prlist([]).

prlist([X|Y]):-
	write(X), nl,
	prlist(Y).

/* Finding Maximum of list*/

max([H|List], Maximum):-
	max(List, H, Maximum).

max([N|List], H, Maximum):-
	H > N,
	max(List, H, Maximum).

max([N|List], H, Maximum):-
	H < N,
	max(List, N, Maximum).

max([], Maximum, Maximum).

/*Determine if length of list is odd or even*/

odd([_|List]):-
	Count = 1,
	odd_count(List, Count).

odd_count([_|List], OldCount):-
	Count = OldCount + 1,
	odd_count(List, Count).

odd_count([], Count):-
	1 is mod(Count,2).

even(List):-
	\+ odd(List).

/*Find the reverse of a list*/

is_reverse(List, ReversedList):-
	is_reverse(List, ReversedList, []).

is_reverse([H|List], ReversedList, CompleteReversedList):-
	NewCRL = [H|CompleteReversedList],
	is_reverse(List, ReversedList, NewCRL).

is_reverse([], ReversedList, ReversedList).

/*Check if two lists are equal*/

is_equal([],[]).

is_equal([H|List], [H2|List2]):-
	H = H2,
	is_equal(List, List2).

/*Check if a list as a palindrome*/

is_palindrome(List):-
	is_reverse(List, ReversedList),
	is_equal(List, ReversedList).

/*Check if list is empty*/

list_empty([], true).
list_empty([_|_], false).

/* Getting list of values in betweem two numbers */

between(N1, N2, []):-
	N2 is N1 + 1.

between(N1, N2, [X|Rest]):-
	X is N1 + 1,
	X < N2,
	between(X, N2, Rest).

/* Combining two lists*/
concat_custom([], L, L).
concat_custom([X|LI], L2, [X|L3]) :-
	concat_custom(LI, L2, L3).

/*Flatten a complex multi-nested list*/
/*Buggy*/

flatten(ComplexList, FlattenedList):-
	flatten(ComplexList, FlattenedList, []).

flatten([H|T], FlattenedList, AccumList):-
    atom(H),
    NewAccumList = [H|AccumList],
    flatten(T, FlattenedList, NewAccumList).

flatten([H|_], FlattenedList, AccumList):-
    flatten(H, FlattenedList, AccumList).

flatten([], FlattenedList, FlattenedList).

/* Replace elements A with B in List*/

replace_elements(OldElem, NewElem, List, NewList):-
    replace_elements(OldElem, NewElem, List, NewList, []).

replace_elements(OldElem, NewElem, [H|T], NewList, MiddleList):-
    H = OldElem,
    NewMiddleList = [NewElem|MiddleList],
    replace_elements(OldElem, NewElem, T, NewList, NewMiddleList).

replace_elements(OldElem, NewElem, [H|T], NewList, MiddleList):-
    NewMiddleList = [H|MiddleList],
    replace_elements(OldElem, NewElem, T, NewList, NewMiddleList).

replace_elements(_, _, [], NewList, NewList).


/* See which values are unifiable*/

unifiable(List, Term, Answer):-
    unifiable(List, Term, Answer, []).

unifiable([H|T], Term, Answer, UnifiedAnswer):-
    not(H \= Term),
    NewUnifiedAnswer = [H|UnifiedAnswer],
    unifiable(T, Term, Answer, NewUnifiedAnswer).

unifiable([_|T], Term, Answer, UnifiedAnswer):-
    unifiable(T, Term, Answer, UnifiedAnswer).

unifiable([], _, Answer, Answer).

/* Remove element from list naively */

delete(Elem, List, AlteredList):-
    delete(Elem, List, AlteredList, []).

delete(_, [], AlteredList, AlteredList).

delete(Elem, [H|T], AlteredList, UnifiedList):-
    not(Elem = H),
    NewAlteredList = [H|UnifiedList],
    delete(Elem, T, AlteredList, NewAlteredList).

delete(Elem, [_|T], AlteredList, UnifiedList):-
    delete(Elem, T, AlteredList, UnifiedList).

/*Remove first occurrence from list using power of prolog...
mad tight*/

delete_eleg(E, [E|TL], TL).

delete_eleg(E, [HL|TL], [HL|NL]):-
    delete_eleg(E, TL, NL).

/*Trying a proper delete without extra terms
Accumulator is pattern matched on input. Base case must
traverse back up because last term depends on it.
*/
delete_all_proper(_, [], []).
delete_all_proper(E, [E|RS], NS):-
	delete_all_proper(E, RS, NS).
delete_all_proper(E, [H|RS], [H|NS]):-
	delete_all_proper(E, RS, NS).

replace_elem_proper(_, _, [], []).
replace_elem_proper(Old, New, [Old|RS], [New|NS]):-
	replace_elem_proper(Old, New, RS, NS).
replace_elem_proper(Old, New, [H|RS], [H|NS]):-
	replace_elem_proper(Old,New, RS, NS).

/* KB for overage */
age(claude, 24).
age(barbara, 23).
age(rui, 15).
age(hoki, 13).

overage(_, [], []).
overage(Age, [P1|People], [P1|Ans]):-
	age(P1,P1_Age),
	P1_Age > Age,
	overage(Age, People, Ans).
overage(Age, [_|People], Ans):-
	overage(Age, People, Ans).


single_elem(X):-
	atom(X).
single_elem(X):-
	number(X).

/*
[a, b, c, d] -> a + (b + (c + d))
*/
list_to_term([X], X):-
	single_elem(X).
list_to_term([H|RS], H+NS):-
	list_to_term(RS, NS).

/*replace all '+' with '-' in sentence*/
inv_list_to_term(X, X):-
	single_elem(X).
inv_list_to_term(H + RS, H - NS):-
	inv_list_to_term(RS, NS).
inv_list_to_term(H - RS, H - NS):-
	inv_list_to_term(RS, NS).


/*
a + b + c -> [a,b,c]

way to do this would be to create list then filter
instead of filtering while creating
*/
numset(X, [X]):-
	single_elem(X).
numset(RS + H, [H|NS]):-
	numset(RS, NS).