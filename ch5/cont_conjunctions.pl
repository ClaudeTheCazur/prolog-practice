likes(sam, coke).
likes(georgia, sprite).
likes(georgia, mountain_dew).
likes(georgia, coke).

bothlike(Person, Other, Drink):-
	likes(Person, Drink),
	likes(Other, Drink).


;;Talk about me 
knows(bill, jane).
knows(jane, pat).
knows(jane, fred).
knows(fred, bill).

talks_about(A,B):-
	knows(A,B).
talks_about(P,R):-
	knows(P,Q),
	talks_about(Q,R).

;; Running talks_about(X,Y) gives us trivial solutions
;; and interesting ones such as
;; X=bill, Y=pat
;; X=jane, Y=bill
;; X=fred, Y=jane
;; X=bill, Y=fred;;;
;; pat is the only one to mind her own business! lol
