%3.1

% a: true
% b: true
% c: true
% d: false
% e: false
% f: false
% g: true
% h: true
% i: true
% j: false

%3.2

% a: true
% b: false
% c: Y = 1
% d: P = point(2, _some_digit)
% e: S=(point(_some_digit_1, some_digit_2), point(_some_digit_1, some_digit_2))

%3.3

    % a: true
    % b: false
    % c: false
    % d: Pam = pat, Liz = jim
    % e: P=pat, C=C2, C=jim


