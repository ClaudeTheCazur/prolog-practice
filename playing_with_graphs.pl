/* playing with graphs */

link(a, b).
link(b, d).
link(d, e).
link(d, f).
link(f, a).
link(d, c).
link(a, c).

path(Start, Start, [Start]).
path(Start, End, [Start| Rest]):-
    link(Start, Mid),
    path(Mid, End, Rest).

concatCustom([], Y, Y).
concatCustom([X| More], Y, [X| Rest]):-
    concatCustom(More, Y, Rest).

findPath(Start, End, Path):-
    concatCustom(Path, _, _),
    path(Start, End, Path).

/*
concatCustom comes up with a solution, and path tries to match that solution!
path = [] fails when start = a, end = c ->
    path = [] fails, since path(a, c, []) fails

path = [X, Y] passes when start = a , end = c ->
    path(a, c, [X, Y]) has values X = a, Y, = c

path = [W, X, Y, Z] passes when start = a, end = c ->
    path(a, c, [W, X, Y, Z]) has values W=a, X=b, Y=d, Z=c

goes on and on
*/