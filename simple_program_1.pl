    hates(claude, milk, wine).

    likes(mike, orange_juice).
    likes(juan, milk).

    drinks(claude, beer).
    drinks(mike, beer).
    drinks(juan, tea).


    % Results:

    % likes(claude, beer).
    % -> true

    % likes(claude, milk).
    % -> false

    % likes(claude, What). %% prog doesn't state what claude likes
    % -> false

    % hates(claude, What). %% matches only fact
    % -> What = milk.

    %% After arity of hates becomes 3

    % hates(claude, What).
    % -> ERROR undefined procedure

    % hates(claude, X, Y).
    % -> X = milk,
    % -> Y = wine.

    % drinks(claude, beer), drinks(juan, tea).
    % -> true

    % drinks(claude, beer), drinks(juan, beer).
    % -> false

    % drinks(claude, beer), drinks(mike, beer).
    % -> true

    % drinks(claude, What), drinks(mike, What).
    % -> What = beer

    % drinks(claude, What), drinks(juan, What).
    % -> false

    % drinks(claude, What), drinks(juan, What2).
    % -> What = beer
    % -> What2 = tea
